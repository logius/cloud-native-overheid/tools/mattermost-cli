package team

import (
	"fmt"
	"log"

	"github.com/Nerzal/gocloak/v8"
	"github.com/mattermost/mattermost-server/v5/model"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/config"
)

func createTeam(mattermostClient *model.Client4, teamName string) (*model.Team, error) {
	team, r := mattermostClient.GetTeamByName(teamName, "")
	if r.Error != nil && r.Error.StatusCode != 404 {
		return nil, fmt.Errorf("checking team %q: %w", teamName, r.Error)
	}
	if team != nil {
		return team, nil
	}
	team = &model.Team{
		Name:        teamName,
		DisplayName: teamName,
		Type:        model.TEAM_INVITE,
	}
	log.Printf("Create team %q", teamName)
	team, r = mattermostClient.CreateTeam(team)
	if r.Error != nil {
		return nil, r.Error
	}

	return team, nil
}

//
// Example rolebinding config is provided below.
// mattermost:
// 	 roles:
// 	 - name: admin
// 	   groups:
// 	   - sample-developer
func configureRoleBindings(mattermostClient *model.Client4, team *model.Team, keycloakGroupMembers map[string][]*gocloak.User, mattermostUsers map[string]*model.User, customerConfig *config.Customer) error {

	// get access level for all users in this team (a role might be mapped to multiple groups)
	mattermostMembers := getAccessLevels(keycloakGroupMembers, customerConfig)

	// update the access level for all users in this team
	for mattermostUserName, accessLevel := range mattermostMembers {

		if err := addMember(mattermostClient, team, mattermostUsers[mattermostUserName], accessLevel); err != nil {
			return err
		}
	}

	return nil
}

func isAccessLevelHigher(currentLevel string, otherLevel string) bool {
	levels := map[string]int{
		"admin":  3,
		"member": 2,
		"guest":  1,
	}

	return levels[otherLevel] > levels[currentLevel]
}

func getAccessLevels(keycloakGroupMembers map[string][]*gocloak.User, customerConfig *config.Customer) map[string]string {

	mattermostMembers := make(map[string]string)
	for _, rolebinding := range customerConfig.Mattermost.Rolebindings {
		for _, keycloakGroupName := range rolebinding.Groups {
			members := keycloakGroupMembers[keycloakGroupName]
			for _, member := range members {
				// check if user has higher accesslevel from membership in another group
				if currentAccessLevel, ok := mattermostMembers[*member.Username]; ok {
					if isAccessLevelHigher(currentAccessLevel, rolebinding.Name) {
						mattermostMembers[*member.Username] = rolebinding.Name
					}
				} else {
					mattermostMembers[*member.Username] = rolebinding.Name
				}
			}
		}
	}
	return mattermostMembers
}

func addMember(mattermostClient *model.Client4, team *model.Team, user *model.User, accessLevel string) error {

	// Always add the user to the team. If the user already exists, the existing member will be returned.
	member, r := mattermostClient.AddTeamMember(team.Id, user.Id)
	if r.Error != nil {
		return r.Error
	}

	isTeamAdmin := accessLevel == "admin"
	isGuest := accessLevel == "guest"
	isUser := accessLevel != "guest"
	if member == nil || member.SchemeAdmin != isTeamAdmin || member.SchemeUser != isUser || member.SchemeGuest != isGuest {
		schemeRoles := model.SchemeRoles{
			SchemeAdmin: isTeamAdmin,
			SchemeUser:  isUser,
			SchemeGuest: isGuest,
		}
		log.Printf("Assign role %q to user %q in team %q", accessLevel, user.Username, team.Name)
		_, r := mattermostClient.UpdateTeamMemberSchemeRoles(team.Id, user.Id, &schemeRoles)
		if r.Error != nil {
			return r.Error
		}
	} else {
		log.Printf("Roles in team %q for user %q already configured (admin = %t)", team.Name, user.Username, isTeamAdmin)
	}
	return nil
}

func cleanupMembers(mattermostClient *model.Client4, team *model.Team, users map[string]*model.User) error {

	pageSize := 20
	moreResults := true
	for page := 0; moreResults; page++ {
		members, r := mattermostClient.GetTeamMembers(team.Id, page, pageSize, "")
		moreResults = len(members) == pageSize
		if r.Error != nil {
			return r.Error
		}
		for _, member := range members {
			if !getUserByUserID(users, member.UserId) {
				user, _ := mattermostClient.GetUser(member.UserId, "")
				if user != nil {
					log.Printf("Remove user %q from team %q", user.Username, team.Name)
					mattermostClient.RemoveTeamMember(team.Id, member.UserId)
				}
			}
		}
	}
	return nil
}

func getUserByUserID(users map[string]*model.User, userID string) bool {
	for _, user := range users {
		if user.Id == userID {
			return true
		}
	}
	return false
}
