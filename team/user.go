package team

import (
	"fmt"
	"log"

	"github.com/Nerzal/gocloak/v8"
	"github.com/mattermost/mattermost-server/v5/model"
)

func createMattermostUsers(mattermostClient *model.Client4, keycloakGroupMembers map[string][]*gocloak.User) (map[string]*model.User, error) {
	var users = make(map[string]*model.User)
	for _, keyCloakUsers := range keycloakGroupMembers {
		for _, keyCloakUser := range keyCloakUsers {
			user, err := addUser(mattermostClient, keyCloakUser)
			if err != nil {
				return nil, err
			}
			users[*keyCloakUser.Username] = user
		}
	}
	return users, nil
}

// 'auth_data' is used by Mattermost to sync the user. The field is filled with the 'createTimestamp' from KeyCloak
// 'createTimestamp' is used as a pseudo unique identifier for a user in KeyCloak.
// The OIDC mapping for Mattermost sends the 'createTimestamp' as the 'id' of the user.
// auth_service is filled with 'gitlab' because in the integration Mattermost assumes KeyCloak == Gitlab.
// see also the Javascript 'create-timestamp-mapper.js' in the Keycloak Ansible role.
func addUser(mattermostClient *model.Client4, keyCloakUser *gocloak.User) (*model.User, error) {

	user, r := mattermostClient.GetUserByUsername(*keyCloakUser.Username, "")
	if r.Error != nil && r.Error.StatusCode != 404 {
		return nil, fmt.Errorf("checking for user %q: %w", *keyCloakUser.Username, r.Error)
	}
	if user != nil {
		if user.DeleteAt != 0 {
			_, r := mattermostClient.UpdateUserActive(user.Id, true)
			if r.Error != nil {
				return nil, r.Error
			}
		}
		return user, nil
	}

	anotherUser, _ := mattermostClient.GetUserByEmail(*keyCloakUser.Email, "")
	if anotherUser != nil {
		return nil, fmt.Errorf("mattermost user %q has email address %q, "+
			"but this email address is already taken by user %q. "+
			"Please cleanup user database in Mattermost first", *keyCloakUser.Username, *keyCloakUser.Email, anotherUser.Username)
	}

	authData := ""
	if keyCloakUser.Attributes != nil {
		timestampArray := (*keyCloakUser.Attributes)["createTimestamp"]
		if len(timestampArray) > 0 {
			authData = timestampArray[0][0:14]
		}
	}
	if authData == "" {
		log.Printf("User %q does not have a proper LDAP timestamp. The user will not be associated with a KeyCloak account.", *keyCloakUser.Username)
	}
	log.Printf("Create user %q", *keyCloakUser.Username)
	user = &model.User{
		Email:       *keyCloakUser.Email,
		Username:    *keyCloakUser.Username,
		FirstName:   *keyCloakUser.FirstName,
		LastName:    *keyCloakUser.LastName,
		AuthData:    &authData,
		AuthService: "gitlab",
	}

	user, r = mattermostClient.CreateUser(user)

	if r.Error != nil {
		return nil, r.Error
	}

	return user, nil
}
