package team

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/Nerzal/gocloak/v8"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/mattermost"
)

type flags struct {
	config *string

	mattermostURL    *string
	keycloakURL      *string
	oidcRealm        *string
	systemAdminGroup *string
	generalTeam      *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureTeam(cmd, &flags)
		},
		Use:   "configure-team",
		Short: "Configure Mattermost team",
		Long:  "This command configures team and memberships in Mattermost.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.mattermostURL = cmd.Flags().String("mattermost-url", "", "Mattermost URL")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.keycloakURL = cmd.Flags().String("keycloak-url", "", "URL of KeyCloak")
	flags.systemAdminGroup = cmd.Flags().String("system-admin-group", "", "Group representing the system administrators")
	flags.generalTeam = cmd.Flags().String("general-team", "", "Team joined by all mattermost users")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("realm")
	cmd.MarkFlagRequired("mattermost-url")
	cmd.MarkFlagRequired("keycloak-url")

	return cmd
}

func configureTeam(cmd *cobra.Command, flags *flags) error {

	username, password := k8s.GetKeycloakSecrets()
	os.Setenv("KEYCLOAK_USERNAME", username)
	os.Setenv("KEYCLOAK_PASSWORD", password)

	mattermostClient := mattermost.CreateMattermostClient(*flags.mattermostURL)
	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	customerConfig := config.LoadConfigfile(*flags.config)

	log.Printf("Configure team for %q", customerConfig.Name)
	team, err := createTeam(mattermostClient, customerConfig.Name)
	if err != nil {
		return err
	}

	keycloakGroupMembers, err := readKeycloakGroups(keyCloakClient, customerConfig.Mattermost.Rolebindings)
	if err != nil {
		return err
	}
	users, err := createMattermostUsers(mattermostClient, keycloakGroupMembers)
	if err != nil {
		return err
	}
	err = configureRoleBindings(mattermostClient, team, keycloakGroupMembers, users, customerConfig)
	if err != nil {
		return err
	}

	err = cleanupMembers(mattermostClient, team, users)
	if err != nil {
		return err
	}

	// might be better to put this in a command on its own
	if hasSystemAdminGroup(keycloakGroupMembers, *flags.systemAdminGroup) {
		err = configureSystemAdmins(mattermostClient, keycloakGroupMembers[*flags.systemAdminGroup])
		if err != nil {
			return err
		}
	}

	if *flags.generalTeam != "" {
		log.Printf("Configure general team %q", *flags.generalTeam)
		err = configureGeneralTeam(mattermostClient, users, *flags.generalTeam)
		if err != nil {
			return err
		}
	}

	if customerConfig.Mattermost.ExtraTeam.Name != "" {
		log.Printf("Configure extra team %q with groups %q", customerConfig.Mattermost.ExtraTeam.Name, customerConfig.Mattermost.ExtraTeam.Groups)

		keycloakGroupMembers, err := readKeycloakGroups(keyCloakClient, []config.Rolebinding{customerConfig.Mattermost.ExtraTeam})
		if err != nil {
			return err
		}
		users, err := createMattermostUsers(mattermostClient, keycloakGroupMembers)
		if err != nil {
			return err
		}
		err = configureGeneralTeam(mattermostClient, users, customerConfig.Mattermost.ExtraTeam.Name)
		if err != nil {
			return err
		}
	}

	return nil
}

func hasSystemAdminGroup(keycloakGroupMembers map[string][]*gocloak.User, systemAdminGroup string) bool {
	for groupName, _ := range keycloakGroupMembers {
		if groupName == systemAdminGroup {
			return true
		}
	}
	return false
}

func readKeycloakGroups(keyCloakClient *keycloak.Client, roleBindings []config.Rolebinding) (map[string][]*gocloak.User, error) {

	keycloakGroupMembers := make(map[string][]*gocloak.User)

	for _, rolebinding := range roleBindings {
		for _, groupName := range rolebinding.Groups {
			var members []*gocloak.User
			var err error
			if groupName == "*" {
				members, err = keyCloakClient.GetAllUsers()
				if err != nil {
					return nil, err
				}
			} else {
				groupID, err := keyCloakClient.GetGroupID(groupName)
				if err != nil {
					return nil, err
				}
				if groupID == "" {
					return nil, fmt.Errorf("could not find group %q in KeyCloak", groupName)
				}
				members, err = keyCloakClient.GetGroupMembers(groupID)
				if err != nil {
					return nil, err
				}
			}
			keycloakGroupMembers[groupName] = getProperConfiguredUsers(members)
		}
	}

	return keycloakGroupMembers, nil
}

func getProperConfiguredUsers(users []*gocloak.User) []*gocloak.User {
	properUsers := make([]*gocloak.User, 0)
	for _, user := range users {
		if user.Email == nil || *user.Email == "" {
			log.Printf("Skip user without email %q", *user.Username)
		} else if strings.HasPrefix(*user.Username, "_") {
			log.Printf("Skip internal user %q", *user.Username)
		} else {
			properUsers = append(properUsers, user)
		}
	}
	return properUsers
}
