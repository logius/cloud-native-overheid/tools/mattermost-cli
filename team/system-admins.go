package team

import (
	"fmt"
	"log"

	"github.com/Nerzal/gocloak/v8"
	"github.com/mattermost/mattermost-server/v5/model"
)

func configureSystemAdmins(mattermostClient *model.Client4, systemAdminUsers []*gocloak.User) error {

	for _, systemAdminUser := range systemAdminUsers {

		mattermostUser, r := mattermostClient.GetUserByUsername(*systemAdminUser.Username, "")
		if r.Error != nil && r.Error.StatusCode != 404 {
			return fmt.Errorf("checking for user %q: %w", *systemAdminUser.Username, r.Error)
		}
		if mattermostUser.IsSystemAdmin() {
			log.Printf("system_admin role is already configured for user %q", *systemAdminUser.Username)
		} else {
			log.Printf("Add system_admin role to user %q", *systemAdminUser.Username)
			_, r = mattermostClient.UpdateUserRoles(mattermostUser.Id, "system_user system_admin")
			if r.Error != nil {
				return r.Error
			}
		}
	}

	return cleanupSystemAdmins(mattermostClient, systemAdminUsers)
}

func cleanupSystemAdmins(mattermostClient *model.Client4, systemAdminUsers []*gocloak.User) error {

	pageSize := 20
	moreResults := true
	for page := 0; moreResults; page++ {
		users, r := mattermostClient.GetUsers(page, pageSize, "")
		moreResults = len(users) == pageSize
		if r.Error != nil {
			return r.Error
		}
		for _, user := range users {
			if !user.IsBot && user.IsSystemAdmin() && !userIsSystemAdmin(systemAdminUsers, user.Username) {
				log.Printf("Remove system_admin role from user %q", user.Username)
				_, r := mattermostClient.UpdateUserRoles(user.Id, "system_user")
				if r.Error != nil {
					return r.Error
				}
			}
		}
	}
	return nil
}

func userIsSystemAdmin(systemAdminUsers []*gocloak.User, userName string) bool {
	for _, systemAdminUser := range systemAdminUsers {
		if *systemAdminUser.Username == userName {
			return true
		}
	}
	return false
}
