package team

import (
	"fmt"
	"log"

	"github.com/mattermost/mattermost-server/v5/model"
)

func configureGeneralTeam(mattermostClient *model.Client4, users map[string]*model.User, generalTeam string) error {

	team, err := createTeam(mattermostClient, generalTeam)
	if err != nil {
		return err
	}
	channels, r := mattermostClient.GetPublicChannelsForTeam(team.Id, 0, 100, "")
	if r.Error != nil {
		return r.Error
	}

	for userName, mattermostUser := range users {

		err = addMember(mattermostClient, team, mattermostUser, "member")
		if err != nil {
			return err
		}
		for _, channel := range channels {
			channelMember, r := mattermostClient.GetChannelMember(channel.Id, mattermostUser.Id, "")
			if r.Error != nil && r.Error.StatusCode != 404 {
				return fmt.Errorf("checking user %q in channel %q for team %q: %w", userName, channel.Name, team.Name, r.Error)
			}
			if channelMember == nil {
				log.Printf("Add user %q to channel %q in general team %q", userName, channel.Name, team.Name)
				_, r = mattermostClient.AddChannelMember(channel.Id, mattermostUser.Id)
				if r.Error != nil {
					return r.Error
				}
			}
		}
	}
	return nil
}
