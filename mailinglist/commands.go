package mailinglist

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/mattermost"
)

type flags struct {
	generalTeam   *string
	mattermostURL *string
	fileName      *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return getMailingList(cmd, &flags)
		},
		Use:   "create-mailinglist",
		Short: "Creates a mailing list of customers.",
		Long:  "Gets all users from the customer project and add them into one single CSV.",
	}

	flags.generalTeam = cmd.Flags().String("general-team", "", "Team joined by all mattermost users.")
	flags.mattermostURL = cmd.Flags().String("mattermost-url", "", "Mattermost URL")
	flags.fileName = cmd.Flags().String("filename", "", "File Name")

	cmd.MarkFlagRequired("general-team")
	cmd.MarkFlagRequired("mattermost-url")
	cmd.MarkFlagRequired("filename")

	return cmd
}

func getMailingList(cmd *cobra.Command, flags *flags) error {

	mattermostClient := mattermost.CreateMattermostClient(*flags.mattermostURL)

	team, r := mattermostClient.GetTeamByName(*flags.generalTeam, "")
	if r.Error != nil {
		return fmt.Errorf("error checking team %q: %w", *flags.generalTeam, r.Error)
	}

	users, r := mattermostClient.GetActiveUsersInTeam(team.Id, 0, 5000, "")
	if r.Error != nil {
		return fmt.Errorf("error checking users in team %q: %w", *flags.generalTeam, r.Error)
	}

	mailingList := ""
	for _, user := range users {
		mailingList += user.Email + ";"
	}

	err := os.WriteFile("./"+*flags.fileName+".txt", []byte(mailingList), 0644)
	if err != nil {
		return err
	}

	return nil
}
