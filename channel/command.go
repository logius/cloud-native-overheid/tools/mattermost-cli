package channel

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/config"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/mattermost"
)

type flags struct {
	config        *string
	mattermostURL *string
	// team          *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureExtraChannels(cmd, &flags)
		},
		Use:   "create-extra-channels",
		Short: "Configure Mattermost Extra Channels",
		Long:  "This command configures any extra channels defined for a team.",
	}

	flags.config = cmd.Flags().String("config", "", "Path to customer configfile")
	flags.mattermostURL = cmd.Flags().String("mattermost-url", "", "Mattermost URL")

	cmd.MarkFlagRequired("config")
	cmd.MarkFlagRequired("mattermost-url")

	return cmd
}

func configureExtraChannels(cmd *cobra.Command, flags *flags) error {

	mattermostClient := mattermost.CreateMattermostClient(*flags.mattermostURL)
	customerConfig := config.LoadConfigfile(*flags.config)

	team, r := mattermostClient.GetTeamByName(customerConfig.Name, "")
	if r.Error != nil {
		return fmt.Errorf("error checking team %q: %w", customerConfig.Name, r.Error)
	}

	for _, channelName := range customerConfig.Mattermost.ExtraChannels {
		fmt.Printf("Adding extra channel to team %s with name: %s\n", customerConfig.Name, channelName)

		channel, err := createChannelIfNotPresent(mattermostClient, channelName, team.Id, false)
		if err != nil {
			return err
		}
		if channel == nil {
			return fmt.Errorf("unable to create channel with name %s for team %s", channelName, customerConfig.Name)
		}

		users, r := mattermostClient.GetActiveUsersInTeam(team.Id, 0, 1000, "")
		if r.Error != nil {
			return fmt.Errorf("unable to get users from team %q: %w", customerConfig.Name, r.Error)
		}

		count, err := addTeamUsersToChannel(mattermostClient, channel.Id, users)
		if err != nil {
			return err
		}
		fmt.Printf("added %v users to %s in team %s \n", count, channel.Name, customerConfig.Name)
	}
	return nil
}
