package channel

import (
	"fmt"

	"github.com/mattermost/mattermost-server/v5/model"
)

func channelModel(channel *model.Channel, name string, teamId string, channelType string) *model.Channel {
	if channel == nil {
		channel = &model.Channel{}
	}

	channel.TeamId = teamId
	channel.Name = name
	channel.DisplayName = name
	channel.Type = channelType
	return channel
}

func createChannel(mattermostClient *model.Client4, name string, teamId string, channelType string) (*model.Channel, error) {

	channel, r := mattermostClient.CreateChannel(channelModel(nil, name, teamId, channelType))
	if r.Error != nil {
		return nil, fmt.Errorf("creating channel failed with %w", r.Error)
	}
	return channel, nil
}

func createChannelIfNotPresent(mattermostClient *model.Client4, name string, teamId string, private bool) (*model.Channel, error) {
	channelType := model.CHANNEL_OPEN
	if private {
		channelType = model.CHANNEL_PRIVATE
	}

	channel, r := mattermostClient.GetChannelByName(name, teamId, "")
	if r.Error != nil && r.Error.StatusCode != 404 {
		return nil, fmt.Errorf("getting channel failed with %d %w", r.StatusCode, r.Error)
	}
	if channel != nil {
		// Make sure that channel fits with correct model
		mattermostClient.UpdateChannel(channelModel(channel, name, teamId, channelType))

		return channel, nil
	}
	channel, err := createChannel(mattermostClient, name, teamId, channelType)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Successfully created channel %s for team %s \n", channel.Name, channel.TeamId)

	return channel, nil
}

func addTeamUsersToChannel(mattermostClient *model.Client4, channelId string, users []*model.User) (int, error) {
	fmt.Printf("Attempting to add users to channel: %s \n", channelId)
	count := 0
	for _, user := range users {
		member, r := mattermostClient.AddChannelMember(channelId, user.Id)
		if r.Error != nil {
			return 0, fmt.Errorf("adding user %s to channel %s failed with %w", user.Username, channelId, r.Error)
		}
		if member != nil {
			count++
		}
	}
	return count, nil
}
