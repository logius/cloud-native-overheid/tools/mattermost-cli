package config

import (
	"io/ioutil"
	"log"

	"gopkg.in/yaml.v3"
)

// Rolebinding defines the binding between role and zero or more groups
type Rolebinding struct {
	Name   string
	Groups []string `validate:"required" yaml:"groups,omitempty"`
}

// Mattermost
type Mattermost struct {
	// authorization with rolebindings follows same model as other tools, see e.g. harbor and rancher
	Rolebindings  []Rolebinding `validate:"required,dive" yaml:"roles"`
	ExtraTeam     Rolebinding   `yaml:"extraTeam"`
	ExtraChannels []string      `yaml:"extraChannels"`
}

// Customer configuration
type Customer struct {
	Name string

	Mattermost Mattermost `validate:"required"`
}

// Config contains customer configuration
type Config struct {
	Customer Customer `validate:"required"`
}

// LoadConfigfile loads the customer configuration from file
func LoadConfigfile(configFile string) *Customer {
	yamlFile, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatalf("Error reading YAML file: %s\n", err)
	}
	return NewConfig(string(yamlFile))
}

// NewConfig gets the customer configuration
func NewConfig(configData string) *Customer {

	var cfg Config
	err := yaml.Unmarshal([]byte(configData), &cfg)
	if err != nil {
		log.Fatalf("Could not parse the config err = %v", err)
	}

	return &cfg.Customer
}
