package cleanup

import (
	"log"
	"os"

	"github.com/Nerzal/gocloak/v8"
	"github.com/mattermost/mattermost-server/v5/model"
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/k8s"
	"gitlab.com/logius/cloud-native-overheid/tools/keycloak-cli/keycloak"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/mattermost"
)

type flags struct {
	mattermostURL *string
	keycloakURL   *string
	oidcRealm     *string
}

// NewCommand creates a new command
func NewCommand() *cobra.Command {

	flags := flags{}

	cmd := &cobra.Command{
		RunE: func(cmd *cobra.Command, args []string) error {
			return configureTeam(cmd, &flags)
		},
		Use:   "cleanup-users",
		Short: "Cleanup Mattermost users",
		Long:  "This command cleans up the user administration.",
	}

	flags.mattermostURL = cmd.Flags().String("mattermost-url", "", "Mattermost URL")
	flags.oidcRealm = cmd.Flags().String("realm", "", "OIDC realm")
	flags.keycloakURL = cmd.Flags().String("keycloak-url", "", "URL of KeyCloak")

	cmd.MarkFlagRequired("realm")
	cmd.MarkFlagRequired("mattermost-url")
	cmd.MarkFlagRequired("keycloak-url")

	return cmd
}

func configureTeam(cmd *cobra.Command, flags *flags) error {

	username, password := k8s.GetKeycloakSecrets()
	os.Setenv("KEYCLOAK_USERNAME", username)
	os.Setenv("KEYCLOAK_PASSWORD", password)

	mattermostClient := mattermost.CreateMattermostClient(*flags.mattermostURL)
	keyCloakClient, err := keycloak.NewKeyCloakClient(*flags.keycloakURL, *flags.oidcRealm)
	if err != nil {
		return err
	}

	log.Printf("Cleanup Mattermost users")
	keycloakUsers, err := readKeycloakUsers(keyCloakClient)

	if err != nil {
		return err
	}

	pageSize := 100
	moreResults := true
	for page := 0; moreResults; page++ {
		users, r := mattermostClient.GetUsers(page, pageSize, "")
		moreResults = len(users) == pageSize

		if r.Error != nil {
			return r.Error
		}

		for _, user := range users {
			if !user.IsOAuthUser() {
				log.Printf("Skip internal user %q", user.Username)
				continue
			}
			if !isKeyCloakUser(keycloakUsers, user) {
				log.Printf("Delete Mattermost user %q", user.Username)
				mattermostClient.DeleteUser(user.Id)
			}
		}
	}
	return nil
}

func isKeyCloakUser(keycloakUsers []*gocloak.User, mattermostUser *model.User) bool {

	for _, keycloakUser := range keycloakUsers {
		if *keycloakUser.Username == mattermostUser.Username {
			return true
		}
	}
	return false
}

func readKeycloakUsers(keyCloakClient *keycloak.Client) ([]*gocloak.User, error) {

	users, err := keyCloakClient.GetAllUsers()
	if err != nil {
		return nil, err
	}

	return users, nil
}
