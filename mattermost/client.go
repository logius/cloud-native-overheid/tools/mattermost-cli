package mattermost

import (
	"log"
	"strings"

	"github.com/mattermost/mattermost-server/v5/model"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/secrets"
)

// CreateMattermostClient creates a new Mattermost client
func CreateMattermostClient(mattermostURL string) *model.Client4 {
	mattermostURL = strings.TrimSuffix(mattermostURL, "/")
	mattermostToken := secrets.GetMattermostToken()

	mattermostClient := model.NewAPIv4Client(mattermostURL)

	makeSureServerIsRunning(mattermostClient)

	mattermostClient.SetToken(mattermostToken)

	return mattermostClient
}

func makeSureServerIsRunning(client *model.Client4) {
	if props, resp := client.GetOldClientConfig(""); resp.Error != nil {
		log.Fatalf("There was a problem pinging the Mattermost server.\n%v", resp.Error)
	} else {
		log.Printf("Mattermost server version " + props["Version"])
	}
}
