#!/bin/bash

IMAGE=registry.gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli:local

docker run --rm -it \
    -v $KUBECONFIG:/root/.kube/config \
    -v $PWD/examples:/examples \
    $IMAGE configure-team --config=examples/tenant.config.yaml
