package commands

import (
	"github.com/spf13/cobra"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/channel"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/mailinglist"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/mattermost/cleanup"
	"gitlab.com/logius/cloud-native-overheid/tools/mattermost-cli/team"
)

// AddMainCommand adds the main command for the current module
func AddMainCommand(rootCmd *cobra.Command) {
	cmd := &cobra.Command{
		Use:   "mattermost",
		Short: "OPS tools for Mattermost",
	}
	rootCmd.AddCommand(cmd)

	AddSubCommands(cmd)
}

// AddSubCommands adds subcommands
func AddSubCommands(cmd *cobra.Command) {
	cmd.AddCommand(team.NewCommand())
	cmd.AddCommand(channel.NewCommand())
	cmd.AddCommand(mailinglist.NewCommand())
	cmd.AddCommand(cleanup.NewCommand())
}
