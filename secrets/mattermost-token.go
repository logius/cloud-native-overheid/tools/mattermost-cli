package secrets

import (
	"context"
	"log"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

func GetMattermostToken() string {

	secretName := "teambot-token"

	k8sClient = initK8sClient()

	opts := metav1.ListOptions{
		LabelSelector: "app.kubernetes.io/instance = mattermost",
	}
	deployments, err := k8sClient.AppsV1().Deployments("").List(context.TODO(), opts)
	if err != nil || len(deployments.Items) == 0 {
		log.Fatalf("Could not find mattermost deployment by label %q", opts.LabelSelector)
	}
	secret, err := k8sClient.CoreV1().Secrets(deployments.Items[0].Namespace).Get(context.TODO(), secretName, metav1.GetOptions{})
	if err != nil {
		log.Fatalf("%v", err)
	}

	token := string(secret.Data["token"])
	if token == "" {
		log.Fatalf("Missing token in secret %q", secretName)
	}
	return token
}
